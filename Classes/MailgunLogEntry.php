<?php


namespace Pl\MailgunBundle\Classes;

use SplFileInfo;


/**
 * Class MailgunLogEntry
 * @package Pl\MailgunBundle\Classes
 * @property \DateTime $date
 * @property string $from
 * @property string $to
 * @property string SplFileInfo
 */
class MailgunLogEntry{
	private $date;
	private $from;
	private $to;
	private $file;

	/**
	 * MailGunLogEntry constructor.
	 * @param $file
	 */
	public function __construct(SplFileInfo $file){
		$this->file = $file;

		$arData = preg_split('#_#', $file->getFilename());

		$this->date = date_create_from_format("Y-m-d-H-i-s", $arData[0]);
		$this->from = $arData[2];
		$this->to = $arData[3];
	}

	/**
	 * @return  \DateTime
	 */
	public function getDate(){
		return $this->date;
	}

	/**
	 * @return string
	 */
	public function getFrom(){
		return $this->from;
	}

	/**
	 * @return string
	 */
	public function getTo(){
		return $this->to;
	}

	/**
	 * @return string
	 */
	public function getContent(){
		return file_get_contents($this->file->getPathname());
	}

	/**
	 * @return string
	 */
	public function getSubject(){
		if(preg_match('#<title>(.+)</title#', $this->getContent(), $m)){
			return $m[1];
		}
		return "";
	}

	public function getFilename(){
		return $this->file->getFilename();
	}



}
