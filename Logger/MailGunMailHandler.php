<?php

namespace Pl\MailgunBundle\Logger;


use FilesystemIterator;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MailGunMailHandler
 * @package Pl\MailgunBundle\Logger;
 * @property ContainerInterface $container
 * @property string defaultFromEmail
 */
class MailGunMailHandler extends StreamHandler
{

	protected $maxFiles;
	protected $filenameFormat;
	protected $dateFormat;
	protected $dir;
	protected $subject;
	protected $from;
	protected $to;


	protected function write(array $record){
		$this->url = $this->getTimedFilename();
		file_put_contents($this->url, $record["message"]);
		chmod($this->url, $this->filePermission);
	}

	public function __construct($dir, $maxFiles = 30, $level = Logger::DEBUG, $filePermission = 0777, $bubble = true, $useLocking = false){
		$this->dir = $dir;
		$this->maxFiles = (int)$maxFiles;
		$this->dateFormat = 'Y-m-d-H-i-s';

		if(!file_exists($dir)){
			mkdir($dir, 0777, true);
		}


		$this->deleteIfGreaterThanMaxFile();
		parent::__construct($this->getTimedFilename(), $level, $bubble, $filePermission, $useLocking);

	}

	/**
	 * @param mixed $from
	 */
	public function setFrom($from){
		$this->from = $from;
	}

	/**
	 * @param mixed $to
	 */
	public function setTo($to){
		$this->to = $to;
	}



	protected function getTimedFilename(){
		$timedFilename = sprintf("%s/%s_%s_%s_%s.html"
			, $this->dir
			, date($this->dateFormat)
			, uniqid()
			, $this->from
			, $this->to
		);
		return $timedFilename;
	}



	protected function deleteIfGreaterThanMaxFile(){
		$arFiles = [];
		if ($handle = opendir($this->dir)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$arFiles[] = $entry;
				}
			}
			closedir($handle);
		}
		usort($arFiles, function($path1, $path2){
			if(file_exists($this->dir . "/" . $path1) && file_exists($this->dir . "/" . $path2)){
				return filemtime($this->dir . "/" . $path1) < filemtime ($this->dir . "/" . $path2);
			}
			return 0;
		});
		for($i = $this->maxFiles - 1; $i < count($arFiles); $i++){
			if(file_exists($this->dir . $arFiles[$i])){
				unlink($this->dir . $arFiles[$i]);
			}
		}
	}



}
