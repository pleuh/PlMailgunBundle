<?php

namespace Pl\MailgunBundle\Logger;


use Monolog\Logger;

/**
 * Class MailGunMailLogger
 * @package Pl\MailgunBundle\Logger
 */
class MailGunMailLogger extends Logger{
	/**
	 * MailGunMailLogger constructor.
	 * @param $handler
	 */
	public function __construct(MailGunMailHandler $handler){
		parent::__construct("mailgun_logger", [$handler]);
	}


	public function logMail($from, $to, $content){
		$this->handlers[0]->setFrom($from);
		$this->handlers[0]->setTo($to);
		$this->info($content);
	}

}
