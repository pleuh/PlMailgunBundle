<?php


namespace Pl\MailgunBundle\Manager;

use Mailgun\Exception\HttpClientException;
use Mailgun\Mailgun;
use Pl\MailgunBundle\Classes\MailgunLogEntry;
use Pl\MailgunBundle\Exception\PlMailgunException;
use Pl\MailgunBundle\Logger\MailGunMailLogger;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class StripeManager
 * @package Pl\MailgunBundle\Manager
 * @property string $apiKey
 * @property Mailgun $mailgun
 * @property MailGunMailLogger $logger
 * @property KernelInterface $kernel
 */
class MailgunManager
{
	protected $mailgun;
	protected $logger;
	protected $kernel;
	protected $apiKey;

	public function __construct($apiKey, MailGunMailLogger $logger, KernelInterface $kernel){
		$this->mailgun = Mailgun::create($apiKey);
		$this->logger = $logger;
		$this->apiKey = $apiKey;
		$this->kernel = $kernel;

	}

	public function setEuDomain(){
		$this->mailgun = Mailgun::create($this->apiKey, 'https://api.eu.mailgun.net');
	}

	private function getDomain($from){
		if(preg_match('#^.+@(.+?)($|>| )#', $from, $m)){
			return $m[1];
		}
		throw new PlMailgunException("MailgunManager, cant find domain with from adress");
	}


	/**
	 * @param $to
	 * @param $content
	 * @param $subject
	 * @param $from
	 * @param string $replyTo
	 * @throws PlMailgunException
	 */
	public function sendMessage($to, $content, $subject, $from, $replyTo = ""){
		$this->sendMessageWithOptions([
			"to" => $to,
			"html" => $content,
			"subject" => $subject,
			"from" => $from,
			"replyTo" => $replyTo,
		]);
	}

	/**
	 * @param $to
	 * @param $content
	 * @param $from
	 */
	public function justLog($to, $content, $from){
		$this->logger->logMail($from, $to, $content);
	}

	/**
	 * @param $options
	 * @throws PlMailgunException
	 */
	public function sendMessageWithOptions($options){
		if(!array_key_exists("from", $options)){
			throw new PlMailgunException("Impossible d'envoyer l'email : vous devez fournir l'option from");
		}
		if(!array_key_exists("to", $options)){
			throw new PlMailgunException("Impossible d'envoyer l'email : vous devez fournir l'option to");
		}
		$subject = array_key_exists("subject", $options) ? $options["subject"] : "";
		$html = array_key_exists("html", $options) ? $options["html"] : "";
		$replyTo = array_key_exists("replyTo", $options) ? $options["replyTo"] : "";

		$from = $options["from"];

		$verifiedOptions = [
			"from" => $from,
			"subject" => $subject,
			"html" => $html,
			"h:Reply-To" => $replyTo,
		];

		if(array_key_exists("to", $options)){
			$verifiedOptions["to"] = $this->sanitizeDestEmail($options["to"]);
			$this->logger->logMail($from, $verifiedOptions["to"], $html);
		}
		if(array_key_exists("cc", $options)){
			$verifiedOptions["cc"] = $this->sanitizeDestEmail($options["cc"]);
		}
		if(array_key_exists("bcc", $options)){
			$verifiedOptions["bcc"] = $this->sanitizeDestEmail($options["bcc"]);
		}


		try{
			$domaine = $this->getDomain($options["from"]);
			$this->mailgun->messages()->send($domaine, $verifiedOptions);
		}
		catch(HttpClientException $e){
			throw new PlMailgunException(sprintf("Impossible d'envoyer l'email, peut être le destinataire n'est pas autorisé dans mailgun ? Message d'erreur : %s", $e->getMessage()));
		}
	}

	public function sanitizeDestEmail($destEmails){
		if(!is_array($destEmails)){
			$destEmails = preg_split('#,;#', $destEmails);
		}

		$destStr = "";
		foreach($destEmails as $destEmail){
			$destStr .= sprintf("%s,", $destEmail);
		}
		$destStr = substr($destStr, 0, strlen($destStr) - 1);

		return $destStr;
	}

	/**
	 * @return MailgunLogEntry[]
	 */
	public function getLastSentMailgunLogEntry(){
		$finder = new Finder();
		$finder->files()->in($this->getMailLogDirectory());


		$arFiles = [];
		foreach($finder as $file){
			$arFiles[] = new MailgunLogEntry($file);
		}
		usort($arFiles, function(MailgunLogEntry $a, MailgunLogEntry $b){
			return $a->getDate() < $b->getDate();
		});
		return $arFiles;
	}

	public function getMailLogDirectory(){
		return $this->kernel->getLogDir()."/mailgun/";
	}

}
